#项目

天和体育官网

#宗旨
- 本项目全部采用传统的经典架构，意在巩固基础。
- 本项目全部自己造轮子，尽量不直接采用开源产品。
- 本项目完全免费，使用前请先知会作者<bu.ru@qq.com>。

#技术栈

###官网

- html5
- bootstrap
- jquery
- css3

- java7
- spring4
- struts2
- mybatis
- freemarker

- netty

- mysql

###论坛

- mongodb




#地址

- 演示地址：<http://tianhe.oschina.mopaas.com>
- 正式地址：<http://www.czsth.com> or <http://www.oacz.org>


#技术

- home    - war spring mvc mysql (spring-data) | mongodb 使用最好的结构，以便单独开源 ~war
- bbs     - rest mongodb jersey dropwizard  ~fatjar
- admin   - struts2 mybatis ~war
- metrics - dropwizard ~jar
- file    - 文件服务