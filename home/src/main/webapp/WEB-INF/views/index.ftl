<#include "commons/header.ftl">
<#include "commons/nav.ftl">

<!--内容区-->
<div class="keypoint bg-green-light">
    <div class="line-big">
        <div class="x8">
            <h1>天和体育</h1>

            <p>追求卓越，逐梦未来！天和，中国体育创意营销领跑者。</p>

            <p>
            <#--<button class="button bg-green icon-angle-double-down">&nbsp;详细介绍</button>-->
                <a class="button bg-green icon-angle-double-down" href="#">&nbsp;详细介绍</a>
            </p>
        </div>
        <div class="x3">
            <br><br>

            <p class="text-center"><!--button-block-->
            <#--<a class="button button-large bg-yellow margin-bottom icon-hand-o-right swing-hover" href="#">-->
            <#--&nbsp;&nbsp;申请加入天和-->
            <#--</a>-->
                <button class="button button-large bg-yellow margin-bottom icon-hand-o-right swing-hover dialogs"
                        data-toggle="click" data-target="#joinUS" data-mask="1" data-width="50%">
                    &nbsp;&nbsp;申请加入天和
                </button>

            <div id="joinUS">
                <div class="dialog">
                    <div class="dialog-head">
                        <span class="close rotate-hover"></span>
                        <strong>欢迎加入天和</strong>
                    </div>
                    <div class="dialog-body">
                        对话框内容</br>
                        对话框内容</br>
                        对话框内容</br>
                        对话框内容</br>
                        对话框内容</br>
                    </div>
                    <div class="dialog-foot">
                    <#--<button class="button dialog-close">取消</button>-->
                        <button class="button bg-green">立即申请</button>
                    </div>
                </div>
            </div>


            </p>
        </div>
        <div class="x1"></div>
    </div>
</div>

<br>

<div class="line-big">
    <div class="x5">
        <div class="banner" data-interval="3">
            <ul class="carousel">
                <li class="item"><img src="images/t1.png"/></li>
                <li class="item"><img src="images/t3.jpg"/></li>
                <li class="item"><img src="images/t6.jpg"/></li>
            </ul>
        </div>
    </div>
    <div class="x5">
        <ul class="list-unstyle height-big padding-bottom">
            <li>
                <span class="badge bg-yellow">通知</span>
                <a href="http://www.czsth.com">[扫描版]公布2014全国定向国家级裁判员培训班考核成绩通知[03日] </a>
            </li>
            <li>
                <span class="badge bg-sub">新闻</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知[03日]</a>
            </li>
            <li>
                <span class="badge bg-sub">通知</span>
                <a href="http://www.czsth.com">举办2015全民健身-情缘冰雪定向节暨“寻找美丽中华”通知[30日]</a>
            </li>
            <li>
                <span class="badge bg-main">新闻</span>
                <a href="http://www.czsth.com">关于对授予卓兵兵定向运动员星级称号人员公示的通知[12日] </a>
            </li>
            <li>
                <span class="badge bg-gray">新闻</span>
                <a href="http://www.czsth.com"> 关于对授予卓兵兵定向运动员星级称号人员公示的通知[27日]</a>
            </li>
            <li>
                <span class="badge bg-sub">新闻</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知[03日]</a>
            </li>
            <li>
                <span class="badge bg-green">新闻</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知[03日]</a>
            </li>
            <li>
                <span class="badge bg-sub">新闻</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知[03日]</a>
            </li>
            <li>
                <span class="badge bg-red">处分</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知[03日]</a>
            </li>
            <li>
                <span class="badge bg-sub">新闻</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知[03日]</a>
            </li>
            <li>
                    <span class="float-right">
                        <a href="#" class="text-red text-small icon-angle-double-right"> more</a>
                    </span>
            </li>
        </ul>
    </div>
    <div class="x2">
    <#include "commons/right.ftl">
    </div>
</div>

<br>

<div class="line-big">
    <div class="x3">
        <img src="images/t2.png" width="100%" height="100%" class="img-border radius-small"/>

        <div class="text-center">定向运动</div>
    </div>
    <div class="x3">
        <img src="images/t4.jpg" width="100%" height="100%" class="img-border radius-small"/>

        <div class="text-center">户外拓展</div>
    </div>
    <div class="x3">
        <img src="images/t5.png" width="100%" height="100%" class="img-border radius-small"/>

        <div class="text-center">趣味运动会</div>
    </div>
    <div class="x3">
        <img src="images/t2.png" width="100%" height="100%" class="img-border radius-small"/>

        <div class="text-center">亲子活动</div>
    </div>

</div>

<#include "commons/footer.ftl">