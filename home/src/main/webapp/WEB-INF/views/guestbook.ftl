<#include "commons/header.ftl">
<#include "commons/nav.ftl">

<div class="line-big">
    <div class="x8  xb2-move">
        <div class="keypoint bg-blue-light radius text-center">
            <h1>留言板</h1>

            <p>请先使用本页留言板进行交流，高逼格的天和论坛目前正在火热筹建中...</p>

            <div class="xb8 xb2-move">
                <div class="progress progress-big progress-striped active">
                    <div class="progress-bar bg-yellow" style="width:100%;">敬请期待</div>
                </div>
            </div>
        </div>
        <div class="ds-thread" data-title="留言板"></div>
    </div>
</div>

<#include "commons/duoshuo.ftl">
<#include "commons/footer.ftl">