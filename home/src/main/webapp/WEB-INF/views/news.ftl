<#include "commons/header.ftl">
<#include "commons/nav.ftl">

<div class="line-big">
    <div class="x3">
        <div class="panel border-sub">
            <div class="panel-head  border-sub bg-mix icon-angle-double-right"><strong>&nbsp;多彩天和</strong></div>
            <div class="panel-body">
                dd
            </div>
        </div>
        <br/>

        <div class="panel border-sub">
            <div class="panel-head  border-sub bg-blue-light icon-angle-double-right"><strong>&nbsp;赛事活动</strong></div>
            <div class="panel-body">
                dd
            </div>
        </div>
    </div>
    <div class="x7">
        <ul class="list-unstyle height-big padding-bottom">

        <#list newsList as news>
            <li>
                <#switch news["category"]>
                    <#case "news">
                        <span class="badge bg-yellow">新闻</span>
                        <#break>
                    <#case "medium">
                        This will be processed if it is medium
                        <#break>
                    <#case "large">
                        This will be processed if it is large
                        <#break>
                    <#default>
                        <span class="badge bg-green">资讯</span>
                </#switch>

                <a href="/news/${news["_id"]}">${news["title"]}</a>
                <span class="float-right">
                    <span class="badge" style="margin-right: 10px">${news["count"]}</span>
                    <a href="/news/${news["_id"]}">2015-03-14</a>
                </span>
            </li>
        </#list>

            <li>
                <span class="badge bg-yellow">通知</span>
                <a href="/news/1">[扫描版]公布2014全国定向国家级裁判员培训班考核成绩通知</a>
                <span class="float-right">
                    <span class="badge" style="margin-right: 10px">8</span>
                    <a href="/news/1">2015-03-14</a>
                </span>
            </li>
            <li>
                <span class="badge bg-sub">新闻</span>
                <a href="/news/2">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知</a>
                <span class="float-right">
                    <span class="badge" style="margin-right: 10px">99+</span>
                    <a href="/news/12">2015-03-14</a>
                </span>
            </li>
            <li>
                <span class="badge bg-sub">通知</span>
                <a href="http://www.czsth.com">举办2015全民健身-情缘冰雪定向节暨“寻找美丽中华”通知 </a>
                <span class="float-right">2015-03-14</span>
            </li>
            <li>
                <span class="badge bg-main">新闻</span>
                <a href="http://www.czsth.com">关于对授予卓兵兵定向运动员星级称号人员公示的通知 </a>
                <span class="float-right">2015-03-14</span>
            </li>
            <li>
                <span class="badge bg-gray">新闻</span>
                <a href="http://www.czsth.com"> 关于对授予卓兵兵定向运动员星级称号人员公示的通知 </a>
                <span class="float-right">2015-03-14</span>
            </li>
            <li>
                <span class="badge bg-sub">新闻</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知</a>
                <span class="float-right">2015-03-14</span>
            </li>
            <li>
                <span class="badge bg-green">新闻</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知</a>
                <span class="float-right">2015-03-14</span>
            </li>
            <li>
                <span class="badge bg-sub">新闻</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知</a>
                <span class="float-right">2015-03-14</span>
            </li>
            <li>
                <span class="badge bg-red">处分</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知</a>
                <span class="float-right">2015-03-14</span>
            </li>
            <li>
                <span class="badge bg-sub">新闻</span>
                <a href="http://www.czsth.com">关于公布2014年全国定向国家级裁判员培训班考核成绩的通知</a>
            </li>
            <li>
                <span class="float-right">
                    <a href="#" class="text-red text-small icon-angle-double-right"> more</a>
                </span>
            </li>
        </ul>
    </div>
    <div class="x2">
    <#include "commons/right.ftl">
    </div>
</div>

<#include "commons/footer.ftl">