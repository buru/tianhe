<!--导航条-->
<button class="button icon-navicon margin-small-bottom" data-target="#navigation-tianhe"></button>
<div class="bg-main bg-inverse radius nav-navicon" id="navigation-tianhe">
    <ul class="nav nav-inline nav-menu nav-split nav-big nav-justified">
    <#--<li class="nav-head">天和</li>-->
        <li><a href="/">首页</a></li>
        <li><a href="/news">天和资讯</a></li>
        <li><a href="#">多彩天和</a></li>
        <li><a href="#">活动预告</a></li>
        <li><a href="#">赛事活动</a></li>
        <li><a href="#">去哪儿玩</a></li>
        <li><a href="/bbs">天和论坛</a></li>
        <li><a href="/about">联系我们</a></li>
    </ul>
</div>

<br/>