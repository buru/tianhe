<div>
    <form>
        <div class="input-group padding-little-top">
            <input type="text" class="input border-red" name="keywords" size="30" placeholder="找天和"/>
            <span class="addbtn"><button type="button" class="button bg-red">搜!</button></span>
        </div>
    </form>
</div>

<br/>

<div class="panel border-sub">
    <div class="panel-head  border-sub bg-red-light icon-angle-double-right"><strong>&nbsp;联系方式</strong></div>
    <div class="panel-body">
        <ul class="list-unstyle height">
            <!--class="icon-qq"-->
            <li>QQ&nbsp;群：<br><strong class="icon-qq">&nbsp;166757932</strong></li>
            <li>卓教练：<br><strong class="icon-phone">&nbsp;13813667797</strong></li>
            <li>陈教练：<br><strong class="icon-phone">&nbsp;18550402827</strong></li>
            <!--<li>陈教练：18550402827</li>-->
        </ul>
    </div>
</div>

<br/>

<div class="panel border-sub">
    <div class="panel-head  border-sub bg-red-light icon-angle-double-right"><strong>&nbsp;友情链接</strong></div>
    <div class="panel-body">
        <ul class="list-unstyle height">
            <li class="icon-paperclip"><a href=""> 中国定向协会</a></li>
            <li class="icon-paperclip"><a href=""> 常州市定向运动协会</a></li>
        </ul>
    </div>
</div>
