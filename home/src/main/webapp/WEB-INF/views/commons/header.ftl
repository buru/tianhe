<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <title>天和体育 - 追求卓越 逐梦未来</title>
    <meta name="author" content="天和">
    <meta name="description"
          content="常州市定向运动协会，常州市天和体育文化有限公司，定向运动，定向越野，亲子活动，户外活动，素质拓展，趣味运动会，自驾游，露营，定向协会，天和体育，体育文化。天河体育。">
    <link rel="stylesheet" href="/stylesheets/pintuer.css">
    <script src="/javascripts/jquery.js"></script>
    <script src="/javascripts/pintuer.js"></script>
    <script src="/javascripts/respond.js"></script>
    <#include "baidu_tongji.ftl">
</head>
<body>
<div class="container">

    <!--页头-->
    <div class="line-big margin-little">
        <div class="xs5 xm8 xb8">
            <a href="//www.czsth.com"><img src="/images/logo.png" alt="天和"/></a>
            <span class="text-big">
                <a href="//www.czsth.com" class="icon-thumbs-o-up"> 常州市天和体育文化有限公司</a>
                <a href="//www.oacz.org" class="icon-thumbs-o-up"> 常州市定向运动协会</a>
            </span>
        </div>
        <div class="xs7 xm4 xb4 padding-top">
            <span class="float-right">
                <a href="#" class="win-homepage icon-home"> 设为首页</a>
                <span class="text-little text-gray">|</span>
                <a href="#" class="win-favorite icon-star"> 加入收藏</a>
                <span class="text-little text-gray">|</span>
                <a href="#" class="text-red text-big icon-phone height-large text-right wobble-hover"> 热线 138 1366
                    7797</a>
            </span>
        </div>
    </div>
