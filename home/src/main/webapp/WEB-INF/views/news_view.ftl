<#include "commons/header.ftl">
<#include "commons/nav.ftl">

<div class="line-big">

    <div class="x3">
        <div class="panel border-sub">
            <div class="panel-head  border-sub bg-mix icon-angle-double-right"><strong>&nbsp;多彩天和</strong></div>
            <div class="panel-body">
                dd
            </div>
        </div>
        <br/>

        <div class="panel border-sub">
            <div class="panel-head  border-sub bg-blue-light icon-angle-double-right"><strong>&nbsp;赛事活动</strong></div>
            <div class="panel-body">
                dd
            </div>
        </div>
    </div>
    <div class="x7">
        <h1 align="center">${news["title"]}</h1>

        <div class="line">
            <ul class="selected-inline border-dashed float-right">
                <li style="border: none"> 作者：${news["creator"]!"admin"} </li>
                <li style="border: none"> 发布时间：${(news["createTime"]!.now)?date} </li>
                <li style="border: none"> 分类：${news["category"]} </li>
                <li style="border: none"> 阅读次数(${news["count"]})</li>
                <li style="border: none"><a href="#pinglun" title="去发表你的看法">评论</a></li>
            </ul>
        </div>

        <div>
            <p>${news["content"]}</p>
            <img src='/images/t6.jpg'/>
        </div>

        <a name="pinglun"> </a>

        <div class="ds-thread" data-title="${news["title"]}"></div>

    </div>
    <div class="x2">
    <#include "commons/right.ftl">
    </div>
</div>

<#include "commons/duoshuo.ftl">
<#include "commons/footer.ftl">