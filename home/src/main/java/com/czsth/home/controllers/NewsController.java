package com.czsth.home.controllers;

import com.czsth.home.repositories.NewsRepository;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by bruce on 2015/3/14.
 */
@Controller
@RequestMapping(method = RequestMethod.GET)
public class NewsController {

    @Autowired
    private NewsRepository newsRepo;

    @RequestMapping(value = "/news")
    public ModelAndView news() {
        final List<Document> newsList = newsRepo.find100ListWithoutContent();
        return new ModelAndView("news", "newsList", newsList);
    }


    @RequestMapping(value = "/news/{newsId}")
    public ModelAndView oneNews(@PathVariable String newsId) {

        Document one = newsRepo.findOneAndUpdateCountById(newsId);

//        String content = String.format(
//                "<div align='center'><h1>%s</h1>%s<br><img src='/images/t6.jpg'/><br>%s</div>",
//                one.getString("title"), one.getString("content"), one.get("_id"));
        return new ModelAndView("news_view", "news", one);
    }

}
