package com.czsth.home.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by bruce on 2015/3/15.
 */
@Controller
@RequestMapping(method = RequestMethod.GET)
public class ActivityController {

    @RequestMapping(value = "/activities/forecast")
    public ModelAndView forecast() {
        return new ModelAndView("null");
    }

    @RequestMapping(value = "/activities/event")
    public ModelAndView event() {
        return new ModelAndView("null");
    }

    @RequestMapping(value = "/activities/where")
    public ModelAndView where() {
        return new ModelAndView("null");
    }
}
