package com.czsth.home.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Auth: bruce-sha
 * Date: 2015/3/18
 */
@Controller
@RequestMapping(method = RequestMethod.GET)
public class BBSController {

    @RequestMapping("/bbs")
    public String d() {
        return "guestbook";
    }
}
