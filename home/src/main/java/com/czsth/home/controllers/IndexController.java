package com.czsth.home.controllers;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by bruce on 2015/3/14.
 * http://blog.sina.com.cn/s/blog_5e09125b0101ag01.html
 */
@Controller
@RequestMapping(method = RequestMethod.GET)
public class IndexController {

    @RequestMapping(value = "/index")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/about")
    public String about() {
        return "about";
    }


    @RequestMapping(value = "/join")
    public String join() {
        return "join";
    }


    // @ModelAttribute
    @RequestMapping(value = "/apply",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ModelAndView apply(Application application) {

        return new ModelAndView("apply");
    }

    public static class Application {
        private String name;
        private String phone;
        private String email;
        private String message;
    }
}
