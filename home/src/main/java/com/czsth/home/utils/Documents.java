package com.czsth.home.utils;

import org.bson.Document;

/**
 * Auth: bruce-sha
 * Date: 2015/3/17
 */
public final class Documents {

    public static Document newDocument(Object... kvs) {
        int length = kvs.length;
        assert length % 2 == 0;

        final Document doc = new Document();
        for (int i = 0; i < length; i = i + 2) {
            doc.append(kvs[i].toString(), kvs[i + 1]);
        }

        return doc;
    }

    public static Document newUpdateDocument(Object... kvs) {
        return newDocument("$set", newDocument(kvs));
    }
}
