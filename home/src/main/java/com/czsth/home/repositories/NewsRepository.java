package com.czsth.home.repositories;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.czsth.home.utils.Documents.newDocument;
import static com.mongodb.client.model.Filters.eq;

/**
 * Created by bruce on 2015/3/16.
 */
@Repository
public class NewsRepository {

    final MongoCollection<Document> news = Repositories.singleton().collection("news");

    public List<Document> findTop10Newses() {
        return news.find().limit(10).into(new ArrayList<Document>());
    }


    public List<Document> find100ListWithoutContent() {
        return news.find()
                .limit(100)
                .filter(newDocument("isPublish", true))
                .sort(newDocument("createTime", -1))
                .projection(newDocument("content", 0))
                .into(new ArrayList<Document>());
    }

    public void check(Document doc) {
        assert doc.containsKey("_id");

        assert doc.containsKey("category");
        assert doc.containsKey("title");
        assert doc.containsKey("content");

        assert doc.containsKey("creator");
        assert doc.containsKey("createTime");

        assert doc.containsKey("count");
        assert doc.containsKey("isPublish");
    }

    public void insert(Document doc) {

        doc.append("count", 1);
        doc.append("creator", "admin");
        doc.append("createTime", new Date());

        news.insertOne(doc);
    }

    public Document findOneAndUpdateCountById(String id) {
        return news.findOneAndUpdate(
                newDocument("_id", new ObjectId(id)),
                newDocument("$inc", newDocument("count", 1)));
    }

    public Document findOneByCount(int count) {
        return news.find(eq("count", count)).first();
    }

//    public News findOneByCount1(int count) {
//        return news.find(eq("count", count), News.class).first();
//    }

    public long count() {
        return news.count();
    }

    public void update() {
        news.updateMany(eq("count", 1),
                new Document("$set", new Document("title", "运动会成功举行了吗")));
    }

    public void update(String id) {
        news.updateMany(eq("_id", id),
                new Document("$set", new Document("title", "运动会成功举行了吗")));
    }


    public static void main(String[] args) {
        NewsRepository dao = new NewsRepository();

        Document doc = new Document("title", "11111运动会成功举行1")
                .append("category", "news1")
                .append("count", 1)
                .append("content", "<p>好，好111！</p>")
                .append("isPublish", true);

        Document one = dao.news.find(new Document("_id", new ObjectId("5507dd4d736b801fbcb39e9e"))).first();
        System.out.println(one);


//        Document o = dao.news.find().projection(new Document("title", 1).append("content", 0)).first();
//        System.out.println(o);
//

        dao.insert(doc);

        dao.news.updateMany(newDocument("isPublish", false), newDocument("$set", newDocument("isPublish", true)));
        dao.update();

        Document d = dao.findOneByCount(1);

        System.out.println(d);

        System.out.println(dao.count());

//        News n = dao.findOneByCount1(1);
//        System.out.println(n);
    }


}
