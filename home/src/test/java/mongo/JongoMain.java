package mongo;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
//import org.jongo.Jongo;
//import org.jongo.MongoCursor;

import java.io.IOException;

/**
 * Auth: bruce-sha
 * Date: 2015/3/16
 */
public class JongoMain {

    public static void main(String[] args) throws IOException {

        MongoClient mongoClient = new MongoClient("114.215.81.46", 27017);

        for(String name:mongoClient.listDatabaseNames())

        System.out.println(name);

//        MongoDatabase db=mongoClient.getDatabase("local");




        MongoDatabase db=mongoClient.getDatabase("buru");

        MongoCollection col=db.getCollection("wahaha");

        Document doc = new Document("name", "MongoDB")
                .append("type", "database")
                .append("count" +
                        "", 1)
                .append("info", new Document("x", 203).append("y", 102));
        col.insertOne(doc);


//        Jongo jongo = new Jongo(mongoClient.getDB("buru"));
//        System.out.println(jongo.getCollection("wahaha").count());

        //将新建立的document保存到collection中去
//        col.insertOne(document);

        System.out.println(col.count());

//        DB db = mongoClient.getDB("tianhe");
//
//        Jongo jongo = new Jongo(db);
//        MongoCollection news = jongo.getCollection("news");
//
//        MongoCursor<News> all = news.find("{name: 'Joe'}").as(News.class);
//        News one = news.findOne("{name: 'Joe'}").as(News.class);
    }
}
