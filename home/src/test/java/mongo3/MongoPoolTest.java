package mongo3;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoOptions;
import com.mongodb.QueryBuilder;
import com.mongodb.ServerAddress;

import java.util.concurrent.atomic.AtomicInteger;

public class MongoPoolTest {
    private static final AtomicInteger successfulCalls = new AtomicInteger();
    private static final AtomicInteger unsuccessfulCalls = new AtomicInteger();

    public static void main(String ... args) throws Exception {
        MongoOptions opts = new MongoOptions();
        opts.threadsAllowedToBlockForConnectionMultiplier = 1;
        opts.connectionsPerHost = 1;

        ServerAddress addr = new ServerAddress("localhost", 27017);
        Mongo mongo = new Mongo(addr, opts);
        DB db = mongo.getDB("mongotest");
        DBCollection collection = db.getCollection("mongotest");

        doSomething(collection);

        System.out.println("Phase 1");
        System.out.println("successful calls: " + successfulCalls.get());
        System.out.println("unsuccessful calls: " + unsuccessfulCalls.get());
//        System.out.println("num cached connections: " + mongo.getConnector().getDBPortPool(addr).available());

        // reset counters
        successfulCalls.set(0);
        unsuccessfulCalls.set(0);

        // sleep a while
        System.out.print("Restart mongod now! Waiting for 10 seconds: ");
        for (int i = 10; i > 0; i--) {
            System.out.print(i + "... ");
            Thread.sleep(1000);
        }
        System.out.println();

        doSomething(collection);

        System.out.println("Phase 2");
        System.out.println("successful calls: " + successfulCalls.get());
        System.out.println("unsuccessful calls: " + unsuccessfulCalls.get());
    }

    private static void doSomething(DBCollection collection) {
        try {
            DBObject result = collection.findOne(QueryBuilder.start("_id").is(1).get());
            successfulCalls.incrementAndGet();
        } catch (Exception e) {
            e.printStackTrace();
            unsuccessfulCalls.incrementAndGet();
        }
    }
}
