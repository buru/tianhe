package mongo3;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.IOException;
import java.util.Arrays;

/**
 * Auth: bruce-sha
 * Date: 2015/3/16
 */
public class Mongo3Main {

    public static void main(String[] args) throws IOException {

//        MongoClient mongoClient = new MongoClient("114.215.81.46", 27017);

        MongoClient mongoClient = new MongoClient(new ServerAddress("114.215.81.46", 27017), Arrays.asList(
                MongoCredential.createCredential("bao", "buru", "12345678".toCharArray()),
                MongoCredential.createCredential("buru", "admin", "buru.mongodb".toCharArray())
        ));

        for (String name : mongoClient.listDatabaseNames())
            System.out.println(name);


        MongoDatabase db = mongoClient.getDatabase("buru");

        MongoCollection col = db.getCollection("wahaha");
        col.dropCollection();

        Document doc = new Document("name", "MongoDB")
                .append("type", "database")
                .append("count" + "", 1)
                .append("html", "<p>Simditor 是团队协作工具 <a href=\"http://tower.im\">Tower</a> 使用的富文本编辑器。</p>\n" +
                        "<p>相比传统的编辑器它的特点是：</p>\n" +
                        "<ul>\n" +
                        "  <li>功能精简，加载快速</li>\n" +
                        "  <li>输出格式化的标准 HTML</li>\n" +
                        "  <li>每一个功能都有非常优秀的使用体验</li>\n" +
                        "</ul>\n" +
                        "<p>兼容的浏览器：IE10+、Chrome、Firefox、Safari。</p>")
                .append("info", new Document("x", 203).append("y", 102));

        col.insertOne(doc);

        Object myDoc = col.find().first();
        System.out.println(myDoc);


        System.out.println(col.count());

    }

}
