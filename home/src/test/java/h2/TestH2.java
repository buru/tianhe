package h2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Auth: bruce-sha
 * Date: 2015/3/4
 */
public class TestH2 {

//    public static void main(String[] a)
//            throws Exception {
//        Class.forName("org.h2.Driver");
//        Connection conn = DriverManager.
//                getConnection("jdbc:h2:E:\\research\\workspace\\H2Test\\db\\test", "sa", "");
//        // add application code here
//        Statement stmt = conn.createStatement();
//        ResultSet rs = stmt.executeQuery("SELECT * FROM TEST ");
//        while (rs.next()) {
//            System.out.println(rs.getInt("ID") + "," + rs.getString("NAME"));
//        }
//        conn.close();
//    }

    public static void main(String[] a) throws Exception {

        Class.forName("org.h2.Driver");
        Connection conn = DriverManager.getConnection("jdbc:h2:mem:demo", "sa1", "");

        Statement stmt = conn.createStatement();

        stmt.executeUpdate("CREATE TABLE TEST_MEM(ID INT PRIMARY KEY,NAME VARCHAR(255));");
        stmt.executeUpdate("INSERT INTO TEST_MEM VALUES(1, 'Hello_Mem');");
        ResultSet rs = stmt.executeQuery("SELECT * FROM TEST_MEM");
        while (rs.next()) {
            System.out.println(rs.getInt("ID") + "," + rs.getString("NAME"));
        }
        conn.close();
    }

}
