package spring;

import org.springframework.util.StringUtils;

import java.util.Arrays;

import static org.springframework.context.ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS;

/**
 * Created by bruce on 2015/3/14.
 */
public class SpringConfigMain {
    public static void main(String[] args) {

        String[] locations = new String[]{
                "classpath:springmvc-servlet.xml",
                "springmvc-servlet.xml,applicationContext.xml",
                "springmvc-servlet.xml applicationContext.xml",
                "springmvc-servlet.xml;applicationContext.xml",
                "springmvc-servlet.xml  applicationContext.xml",
                "springmvc-servlet.xml \n applicationContext.xml"
        };

        for (String location : locations) {
            System.out.println(
                    Arrays.asList(
                            StringUtils.tokenizeToStringArray(location, CONFIG_LOCATION_DELIMITERS)
                    )
            );
        }
    }
}
