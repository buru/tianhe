package repository;

import com.czsth.home.repositories.NewsRepository;
import org.bson.Document;
import org.junit.Before;
import org.junit.Test;

import static com.czsth.home.utils.Documents.newDocument;

/**
 * Auth: bruce-sha
 * Date: 2015/3/18
 */
public class InitRepository {

    NewsRepository newsRep;

    @Before
    public void before() {
        newsRep = new NewsRepository();
    }


    @Test
    public void initNews() {

        Document news = newDocument("title", "常州市定向协会首次定向越野活动圆满成功")
                .append("category", "news")
                .append("count", 1)
                .append("content", "<p>3月15日，阳光明媚，天空飘着朵朵白云，好，好！</p>")
                .append("isPublish", true);

        newsRep.insert(news);

    }
}
