package com.czsth.home.models;

import org.bson.Document;

import java.util.Date;

/**
 * Created by bruce on 2015/3/15.
 */
public class News extends Document {

    private String _id;

    private String category;
    private String title;
    private String content;

    private String creator;
    private Date createTime;
    private String updater;
    private Date updateTime;

    private int count;

    private boolean isPublish;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isPublish() {
        return isPublish;
    }

    public void setPublish(boolean isPublish) {
        this.isPublish = isPublish;
    }
}
