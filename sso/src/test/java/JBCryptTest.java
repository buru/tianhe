import org.apache.commons.codec.binary.Base64;
import org.mindrot.jbcrypt.BCrypt;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Auth: bruce-sha
 * Date: 2015/3/13
 */
public class JBCryptTest {
    public static void main(String[] args) {
        String password = "123";
        // Hash a password for the first time
        String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
        System.out.println(hashed);

        // gensalt's log_rounds parameter determines the complexity
        // the work factor is 2**log_rounds, and the default is 10
        String hashed2 = BCrypt.hashpw(password, BCrypt.gensalt(12));
        System.out.println(hashed2);

        String candidate = "123";
        // Check that an unencrypted password matches one that has previously been hashed
        if (BCrypt.checkpw(candidate, hashed))
            System.out.println("It matches");
        else
            System.out.println("It does not match");

//        AES.
        String bc=               BCrypt.hashpw("123", BCrypt.gensalt());
        System.out.println(
bc
        );

        String x = Base64.encodeBase64URLSafeString(bc.getBytes(StandardCharsets.UTF_8));

        System.out.println(x);

        byte[] bs=Base64.decodeBase64(x);

        System.out.println(new String(bs,StandardCharsets.UTF_8));
    }
}
