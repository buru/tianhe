package com.czsth.security;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Auth: bruce-sha
 * Date: 2015/3/13
 */
public class HelloAction extends ActionSupport {

    public String hello() {
        return SUCCESS;
    }
}