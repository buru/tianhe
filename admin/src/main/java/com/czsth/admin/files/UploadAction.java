package com.czsth.admin.files;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 处理文件上传的Action
 * @author dove
 */
public class UploadAction  extends ActionSupport {
	/** 代表上传的文件内容的对象 */
	private File upload;

	/** Struts2约定的代表上传的文件的名 */
	private String uploadFileName;
	/** Struts2约定的代表上传文件的内容类型(MIME) */
	private String uploadContentType;

    private String summery;

	public String execute() throws Exception{
		System.out.println("文件的名：" + uploadFileName);
		System.out.println("不要用upload.getName()来获取文件名，这个是临时名：" + upload.getName());
		System.out.println("文件的内容类型：" + uploadContentType);

		//////////使用IO流来操作upload属性
		//File destPath = new File("d:/"); //服务端存放文件的目录

		//如果要存放到web服务器中本项目的某个目录下
		//根据服务器的文件保存地址和原文件名创建目录文件全路径
		String destPath = ServletActionContext.getServletContext().getRealPath("/uploads");

		File dest = new File(destPath, uploadFileName); //服务器的文件

		FileUtils.copyFile(upload, dest);//完成了文件的拷贝工作

		return "succ";
	}

	public String getSummery() {
		return summery;
	}
	public void setSummery(String summery) {
		this.summery = summery;
	}
	public File getUpload() {
		return upload;
	}
	public void setUpload(File upload) {
		this.upload = upload;
	}
	public String getUploadFileName() {
		return uploadFileName;
	}
	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}
	public String getUploadContentType() {
		return uploadContentType;
	}
	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}
}