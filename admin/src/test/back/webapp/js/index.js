/*
 * 主页加载方法
 * @bruce-sha
 **/
// 系统时间显示
// setInterval("document.getElementById('nowTime').innerHTML=new Date().toLocaleString()+' 星期'+'日一二三四五六'.charAt(new Date().getDay());",1000);

$.ajaxSetup({cache: false})

var setting = {
    data: {
        simpleData: {
            enable: true
        }
    },
    view: {
        selectedMulti: false,
        showLine: false
    },
    callback: {
        onClick: function (e, id, node) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree");
            if (node.isParent) {
                zTree.expandNode();
            } else {
                addTabs(node.name, node.file);
            }
        }
    }
};

//var zNodes = [
//    {id: "990", pId: "1110", name: "云之家D2O", open: true},
//    {id: 991, pId: 1990, name: "消息系统", file: "admin/process.html?module=message"},
//    {id: 992, pId: 990, name: "用户系统", file: "admin/process.html?module=open"},
//    {id: 880, pId: 0, name: "云之家部落", open: true},
//    {id: 881, pId: 880, name: "企业微博", file: "admin/process.html?module=weibo"},
//
//    {id: 1, pId: 0, name: "系统管理", open: true},
//    {id: 11, pId: 1, name: "用户管理", file: "user/user.html"},
//    {id: 12, pId: 1, name: "数据备份", file: "backup.html"},
//    {id: 13, pId: 1, name: "权限管理", file: "authority.html"},
//    {id: 14, pId: 1, name: "角色管理", file: "role.html"},
//    {id: 2, pId: 0, name: "父节点", open: false},
//    {id: 21, pId: 2, name: "子节点21", file: ""},
//    {id: 22, pId: 2, name: "子节点22", file: ""},
//    {id: 23, pId: 2, name: "子节点23", file: ""}
//];

var configZNodes = [
    //{ id:1000, pId:0, name:"机器", file:"admin/server.html"},
    {id: 1000, pId: 0, name: "机器", file: "/views/server_list"},
    {id: 1001, pId: 0, name: "产品集", file: "/views/productset_list"},
    //{ id:1002, pId:0, name:"产品_old", file:"admin/product.html"},
    {id: 10021, pId: 0, name: "产品", file: "/views/product_list"},
    {id: 1002, pId: 0, name: "模块", file: "/views/module_list"},
    {id: 1003, pId: 0, name: "进程", file: "/views/process_list"}
];

function initNodes(url, callback) {
    $.ajax({
        type: "GET",
        url: url,
        success: function (zNodes) {
            callback(zNodes);
        },
        error: function (response) {
            //$.messager.alert('错误', '删除' + row.name + '失败：' + response.responseText, 'error');
        }
    });
}

$(function () {

    initNodes("/bizview/nodes", function (nodes) {
        $.fn.zTree.init($("#menuTree"), setting, nodes);
        var zTree = $.fn.zTree.getZTreeObj("menuTree");
        //中间部分tab
        $('#tabs').tabs({
            border: false,
            fit: true,
            onSelect: function (title, index) {
                var treeNode = zTree.getNodeByParam("name", title, null);
                zTree.selectNode(treeNode);
            }
        });
    });


    $.fn.zTree.init($("#configMenuTree"), setting, configZNodes);
    var configZTree = $.fn.zTree.getZTreeObj("configMenuTree");

    $('.index_panel').panel({
        width: 300,
        height: 200,
        closable: true,
        minimizable: true,
        title: 'My Panel'
    });

});

//添加一个选项卡面板 
function addTabs(title, url, icon) {
    if (!$('#tabs').tabs('exists', title)) {
        $('#tabs').tabs('add', {
            title: title,
            content: '<iframe id="child" src="' + url + '" frameBorder="0" border="0" scrolling="no" style="width: 100%; height: 100%;"/>',
            closable: true
        });
    } else {
        $('#tabs').tabs('select', title);
    }
}