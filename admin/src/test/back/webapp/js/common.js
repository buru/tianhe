/*
 公共方法文件
 @bruce-sha
 */
var $parent = self.parent.$;

$(function () {
    //隐藏显示查询条件区域
    $('#openOrClose').on("click", function () {
        $('#conditon').toggle(80);
        setTimeout(domresize, 100);//条件隐藏，改变表格高度
    });


})

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


//$.fn.serializeObject = function () {
//    var o = {};
//    var a = this.serializeArray();
//    $.each(a, function () {
//        if (o[this.name] !== undefined) {
//            if (!o[this.name].push) {
//                o[this.name] = [o[this.name]];
//            }
//            o[this.name].push(this.value || '');
//        } else {
//            o[this.name] = this.value || '';
//        }
//    });
//    debugger;
//    return JSON.stringify(o);
//};