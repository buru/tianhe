/**
 * Created by bruce_sha on 2015/1/12.
 */

function $get(url, callback) {
    $.ajaxSetup({cache: false});
    $.ajax({
        type: "GET",
        url: "/rest/admin/productsets/" + row.id,
        success: function (result) {
            callback();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("操作失败!");
        }
    });
}

function add_product_set() {
    $.ajax({
        type: "POST",
        url: "/rest/admin/productsets",
        contentType: "application/json",
        dataType: "json",
        data: $("form").serializeObject(),
        success: function (result) {
            //$("#child")[0].contentWindow.reload();

            //$("#parent_win").window("close");
            $("#child_win").window("close");
        }
    });
}

function delete_product_set(row) {
    $.ajax({
        type: "GET",
        url: "/rest/admin/productsets/" + row.id,
        success: function () {
            reload();
        },
        error: function () {
            alert('删除' + row.name + '失败!');
        }
    });
}


function add_server() {
    var frm = $("form");
    var data = JSON.stringify(frm.serializeObject());

    $.ajax({
        type: "POST",
        url: "/rest/admin/servers",
        contentType: "application/json",
        // dataType: "json",
        data: data,
        success: function (result) {
            alert(result);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(XMLHttpRequest.status);
            alert(XMLHttpRequest.readyState);
            alert(textStatus);
            alert(errorThrown);
        }
    });

    alert(data)
    alert(frm.serializeObject())
}


$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return JSON.stringify(o);
};

/**
 $(function() {
    $('form').submit(function() {
        $('#result').text(JSON.stringify($('form').serializeObject()));
        return false;
    });
});
 */