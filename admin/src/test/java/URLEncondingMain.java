import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Auth: bruce-sha
 * Date: 2015/3/13
 */
public class URLEncondingMain {
    public static void main(String[] args) {
        try {
            System.out.println(
                    URLEncoder.encode("iphone型号.jpg","UTF-8")
            );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
